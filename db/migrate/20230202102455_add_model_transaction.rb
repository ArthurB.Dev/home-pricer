# frozen_string_literal: true

# Add new table for transactions
class AddModelTransaction < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.datetime :transaction_date
      t.string :address
      t.string :estate_type
      t.string :total_area
      t.integer :number_of_rooms
      t.decimal :value

      t.references :city
      t.timestamps
    end
  end
end
