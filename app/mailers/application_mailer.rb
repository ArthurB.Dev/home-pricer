# frozen_string_literal: true

# TO BE DEFINED
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
