# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TransactionsController, type: :controller do
  render_views

  describe 'get :index' do
    subject { get :index, params: { zip_code: zip_code } }

    let(:zip_code) { nil }

    it 'raises an error' do
      expect { subject }.to raise_error ActionController::ParameterMissing
    end

    context 'with a zip code' do
      let(:zip_code) { '59110' }

      it { is_expected.to have_http_status(:ok) }

      it 'calls fetch' do
        expect(controller).to receive(:fetch)
        subject
      end

      context 'with a city' do
        let!(:city) { create(:city, postal_code: zip_code) }

        it { is_expected.to have_http_status(:ok) }

        it 'shows no transaction' do
          subject
          expect(response.body.squish).to include(
            '<table border=1> <tr> ' \
            '<th>Date</th> <th>Adresse</th> <th>Type</th> <th>Surface bâti</th> ' \
            '<th>Nombre pièces</th> <th>Valeur</th> ' \
            '</tr> </table>'
          )
        end

        context 'with transactions for the city' do
          before { create(:transaction, city_id: city.id) }

          it 'shows the cities transactions' do
            subject
            expect(response.body.squish).to include(
              '<table border=1> <tr> ' \
              '<th>Date</th> <th>Adresse</th> <th>Type</th> <th>Surface bâti</th> ' \
              '<th>Nombre pièces</th> <th>Valeur</th> </tr> ' \
              '<tr> <td>2022-02-24 00:00:00 UTC</td> <td>1 rue de la paix</td> <td>Maison</td> <td>250</td> ' \
              '<td>6</td> <td>200000.0 €</td> ' \
              '</tr> </table>'
            )
          end
        end
      end
    end
  end

  describe 'post :fetch' do
    subject { post :fetch, params: { zip_code: zip_code } }

    let(:zip_code) { nil }

    it 'raises an error' do
      expect { subject }.to raise_error ActionController::ParameterMissing
    end

    context 'with a zip code' do
      let(:zip_code) { '59110' }
      let!(:city) { create(:city, postal_code: zip_code) }

      it { is_expected.to have_http_status(:ok) }

      it 'fetches the citys transactions' do
        allow(City).to receive(:find_or_initialize_by).with(postal_code: zip_code).and_return(city)
        expect(city).to receive(:fetch_transactions!).and_return(true)
        subject
      end

      it 'renders index' do
        is_expected.to render_template(:index)
      end
    end
  end
end
